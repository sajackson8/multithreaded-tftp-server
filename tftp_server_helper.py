#######################################################################################################################
# NAME: Sha Jackson
# COURSE: CSCE 365 - Computer Networks
# DATE MODIFIED: 05 DEC 2020
#
# PURPOSE: The following program implements a TFTP server that is capable of communicating with multiple clients via
#          multithreading.
#
# DESCRIPTION: This file handles the work done by the server including listening, threading, and packet handling.
#
#######################################################################################################################

import socket
import threading
import queue
import os
import time

BLOCK_LENGTH = 516  # Max packet size in TFTP is 516 bytes
TIMEOUT_S = 5  # Timeout (seconds)
MAX_RETRANSMITS = 12  # Max number of times a packet can be retransmitted

# Dict to access error messages
ERROR_MESSAGES = {
    0: b'Not defined, see error message (if any).',
    1: b'File not found.',
    2: b'Access violation',
    3: b'Disk full or allocation exceeded',
    4: b'Illegal TFTP operation',
    5: b'Unknown transfer ID',
    6: b'File already exists',
    7: b'No such user'
}

# Error codes represented as byte arrays (size 2)
ERROR_CODES = {
    0: b'\x00\x00',
    1: b'\x00\x01',
    2: b'\x00\x02',
    3: b'\x00\x03',
    4: b'\x00\x04',
    5: b'\x00\x05',
    6: b'\x00\x06',
    7: b'\x00\x07'
}

# Dict to access opcodes as byte arrays
OP_CODES = {
    'READ': b'\x00\x01',  # RRQ
    'WRITE': b'\x00\x02',  # WRQ
    'DATA': b'\x00\x03',  # DATA
    'ACKNOWLEDGEMENT': b'\x00\x04',  # ACK
    'ERROR': b'\x00\x05',  # ERROR
}

class tftp_server():

    # Initialize server object
    def __init__(self, self_address: str, self_port: int):

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # Initialize server socket using UDP
        self.sock.bind((self_address, self_port))  # Bind server to localhost address and server port
        self.sock.settimeout(TIMEOUT_S)  # Set socket timeout time to TIMEOUT_S
        self.connections = dict()  # Used to store currently open connections
        self.mutex = threading.Lock()  # Mutex to lock threads while a file is open (prevents race conditions when writing to / reading from files)

    # NOT USED BY SERVER
    # When reading form the server, if the filename already exists in client directory, append _i such that filename_i does not exist
    def validate_filename(self, filename):

        def find_ext(file):
            last_dot = file.rfind(".")  # Find where extension of file starts (if file has extension)
            if last_dot < 0:
                last_dot = len(file) - 1
            return last_dot

        increment = 1

        index = find_ext(filename)

        new_filename = filename

        while os.path.isfile(
                new_filename):  # Continue to increment filename appenditure until the filename does not exist
            new_filename = filename[:index] + '_' + str(increment) + filename[index:]
            increment += 1

        return new_filename

    # Ends a connection, terminating the thread and removing the connection from the server's connection dictionary
    def kill_connection(self, id):
        print("Killing connection: ", id)
        del self.connections[id]

    # Waits to receive a packet. 517 bytes to ensure that the packet sent did not exceed the max packet size (516)
    def receive(self):  # Recvfrom is placed in a separate method to avoid blocking-method issues
        return self.sock.recvfrom(517)  # Receives 517 bytes to ensure that no package sent is too large for tftp

    # NOT USED BY SERVER
    # Sends RRQ or WRQ based on the mode selected by the user
    def send_request(self, mode: chr, filename: str, destination):
        time.sleep(.3)  # Sleep for .3 seconds to allow the server to 'wake up'

        rq = bytearray()  # Request packet

        if mode == 'r':  # Append the RRQ/WRQ op code tp packet
            rq += (OP_CODES['READ'])
        else:
            rq += (OP_CODES['WRITE'])

        filename = bytearray(filename.encode('utf-8'))  # Append filename to packet
        rq += filename

        rq.append(0)  # Append \x00 to packet

        mode = bytearray('netascii'.encode('utf-8'))  # Append mode (netascii) to packet
        rq += mode

        rq.append(0)  # Append \x00 to packet

        self.prev_packet = rq
        self.sock.sendto(rq, destination)  # Sends request packer to server

    # Sends data packet
    def send_data(self, data_queue, block_num, destination, is_test=False):

        block_num = ((int.from_bytes(block_num, byteorder='big')) + 1).to_bytes(2, 'big')  # Block num == previously sent block num + 1
        packet = b''  # Initialize packet
        packet += OP_CODES['DATA']  # Append data opcode
        packet += block_num  # Append block number (2 bytes regardless)
        packet += data_queue.get()  # Append the next segment of data from the data queue

        if not is_test:  # Does not send any data when unit testing
            self.sock.sendto(packet, destination)  # Send data packet

        return packet  # Return this sent packet to track our most recently sent packet

    # Sends ack packet
    def send_ack(self, block_num, destination, is_test=False):

        ack = bytearray()  # Initialize ack packet
        ack += OP_CODES['ACKNOWLEDGEMENT']  # Append ack opcode
        ack += block_num  # Append block num obtained from last packet sent from server

        if not is_test:  # Does not send any data when unit testing
            self.sock.sendto(ack, destination)  # Send ack

        return ack  # Return this sent ack to track our most recently sent packet

    # Sends error packet
    def send_err(self, err_code, destination, err_msg=b'', is_test=False):
        err = bytearray()  # Initialize error packet
        err += OP_CODES['ERROR']  # Append err opcode
        err += ERROR_CODES[err_code]  # Append error code
        err += ERROR_MESSAGES[err_code]  # Append error message
        err += err_msg  # Include custom error message if one is present
        err.append(0)  # Append trailing 0

        if not is_test:  # Does not send any data when unit testing
            print("Error with error code", err_code, "sent. Error Message:", ERROR_MESSAGES[err_code].decode(),"\nExiting Program...")
            self.sock.sendto(err, destination)  # Send error

        return err  # Return the error packet sent (only relevant when unit testing)

    # Handles the most recent packet received
    def handle_packet(self, data, data_queue, prev_packet, destination, is_test=False):

        # Error if packet exceeds max length (516 bytes)
        if len(data) > BLOCK_LENGTH:
            return 0, self.send_err(4, destination, is_test=is_test)

        # Error if packet is less than 2 bytes
        if len(data) < 2:
            return 0, self.send_err(4, destination, b'\n ERROR: Invalid Opcode (less than 2 bytes)', is_test=is_test)

        # Get the opcode from the packet
        op = data[:2]

        # Error if opcode is not 1 - 5
        if int.from_bytes(op, byteorder='big') < 1 or int.from_bytes(op, byteorder='big') > 5:
            return 0, self.send_err(4, destination, b'\n ERROR: Opcode must be in range 1 to 5', is_test=is_test)

        # If the opcode is 4, send data
        if op == OP_CODES['ACKNOWLEDGEMENT']:

            # Error if ack does not include 2 byte block number
            if len(data[2:]) != 2:
                return 0, self.send_err(4, b'\n ERROR: Invalid ACK (ack must have a 2 byte block number)', is_test=is_test)

            # If any ack received has a block number other than 0 that is less than the block number of the most recent data package sent, retransmit previous data pack
            elif 0 < int.from_bytes(data[2:], byteorder='big') < int.from_bytes(prev_packet[2:4], byteorder='big'):
                return 2, prev_packet

            # Return 1 for non error packet, return data packet in case resending is needed
            else:
                return 1, self.send_data(data_queue, data[2:], destination, is_test=is_test)

        # If the opcode is 3, receive data
        elif op == OP_CODES['DATA']:

            # If any data pack received has a block number equal to our most recent ack, retransmit previous ack
            if int.from_bytes(data[2:4], byteorder='big') <= int.from_bytes(prev_packet[2:4], byteorder='big'):
                return 2, prev_packet

            # Return 1 for non error packet, return ack packet in case resending is needed
            else:
                data_queue.put(data[4:])  # Add data to the data_queue, so that it can be written to the file once connection is complete
                return 1, self.send_ack(data[2:4], destination, is_test=is_test)

        # If the opcode is 5, exit with error
        elif op == OP_CODES['ERROR']:
            err_code = int.from_bytes(data[2:4], byteorder='big')
            if err_code in range(0, 8):
                print("ERROR:", ERROR_MESSAGES[err_code].decode())  # Print appropriate error message
            else:
                print("ERROR:", ERROR_MESSAGES[0].decode())
            return 0, None  # Return 0 to signal error has occurred

        # Send undefined error if packet makes it this far
        return 0, self.send_err(0, destination, is_test=is_test)

    # Main function for the server
    def listen(self):

        # Receives Packet
            # Starts thread upon receiving request
            # Queues packet to be received by thread if connection already exists
            # TID Error if non-request packet was received by an unknown sender

        # Boolean to note if shutdown.txt has been received or not
        shutdown = False

        # Sleep for 2 seconds to ensure that initial packet isn't missed (tester only works with this enabled)
        time.sleep(2)

        # Will loop until shutdown is received and threads are finished
        while True:

            try:

                # Stop looping if threads are finished and shutdown.txt has been received
                if threading.active_count() < 1 and shutdown:
                    break

                # Listen for packets/requests
                data, client = self.receive()

                # If packet is received from unfamiliar client, check to see if it is a request
                if client not in self.connections.keys():

                    # Upon receiving a read request (if the server is not waiting to shutdown)
                    if data[:2] == OP_CODES['READ'] and not shutdown:

                        # Parse the filename from the packet
                        filename = data[2:len(data) - 10]
                        filename = filename.decode("utf-8")
                        filename = filename[filename.rfind('/') + 1:]

                        # If the filename is shutdown.txt, stop receiving requests
                        if filename == "shutdown.txt":
                            shutdown = True

                        # Otherwise, create a new thread to handle the connection
                        else:
                            # Threads run on the server_run() function with the given filename and client info.
                            # Each thread is stored in the server connections dictionary where the connection info is
                            # the key and the object is a tuple containing: a reference to the thread, a queue used to
                            # send packets to the thread, and a list that stores the same packets (used to ensure that
                            # we don't queue up the same packet multiple times).
                            t = threading.Thread(target=self.server_run, args=('r', filename, client))
                            t.start()
                            self.connections[client] = (t, queue.Queue(), [])

                    # Upon receiving a read request (if the server is not waiting to shutdown)
                    elif data[:2] == OP_CODES['WRITE'] and not shutdown:

                        # Parse the filename from the packet
                        filename = data[2:len(data) - 10]
                        filename = filename.decode("utf-8")
                        filename = filename[filename.rfind('/') + 1:]

                        # Don't check for shutdown.txt if receiving a WRQ

                        # Threads run on the server_run() function with the given filename and client info.
                        # Each thread is stored in the server connections dictionary where the connection info is
                        # the key and the object is a tuple containing: a reference to the thread, a queue used to
                        # send packets to the thread, and a list that stores the same packets (used to ensure that
                        # we don't queue up the same packet multiple times).
                        t = threading.Thread(target=self.server_run, args=('w', filename, client))
                        self.connections[client] = (t, queue.Queue(), [])
                        t.start()

                    # If receiving a non request packet from an unfamiliar client, send an unexpected TID error
                    else:
                        print("Packet from Unexpected TID,", client, "not accepted.")
                        err = bytearray()  # Initialize error packet
                        err += OP_CODES['ERROR']  # Append err opcode
                        err += ERROR_CODES[5]  # Append error code
                        err += ERROR_MESSAGES[5]  # Append error message
                        err.append(0)  # Append trailing 0
                        self.sock.sendto(err, client)  # Send error

                # If we received a packet from a known client, store the packet in the appropriate queue
                else:
                    if data not in self.connections[client][2]:  # Only store the packet if it is not a repeat
                        self.connections[client][1].put(data)
                        self.connections[client][2].append(data)

            except:  # Loop again if the server times out
                continue

        # Terminate the server once shutdown.txt is received and there are no active threads
        self.sock.close()
        print("Server Closing... Goodbye~~")
        exit(0)

    # Main thread process (receives and handles packets for each unique connection made to the server)
    def server_run(self, mode, filename, destination):

        # If we are satisfying a RRQ, we need to read the file from the tftp_files folder
        if mode == 'r':
            filename = os.path.relpath("tftp_files/{}".format(filename))

        # Data queue stores the data read from the file or received from the client
        data_queue = queue.Queue()

        # If in read mode, attempt to open the file, sending an error if the file does not exist
        # If the file can be opened, lock the mutex and store up to 512 byte sections of data in the data_queue.
        # Unlock the mutex and close the file once all data is stored and ready to be sent.
        # Send the initial set of data to satisfy the handling of the RRQ packet.
        if mode == 'r':
            if not os.path.isfile(os.path.abspath(filename)):  # Send error if the file that is being read from does not exist
                self.send_err(1, destination)
                return self.kill_connection(destination)
            else:
                self.mutex.acquire()
                try:
                    in_file = open(filename, "rb")
                    data = in_file.read(512)
                    while len(data) == 512:
                        data_queue.put(data)
                        data = in_file.read(512)
                    data_queue.put(data)
                    in_file.close()
                finally:
                    self.mutex.release()
                    prev_packet = self.send_data(data_queue, b'\x00\x00', destination)
        else:
            prev_packet = self.send_ack(b'\x00\x00', destination)

        # Keeps track of how many times the same packet has been retransmitted
        retransmits = 0

        # Loop until the tftp end connection rule is satisfied (See below)
        while True:

            try:

                # Wait up to TIMEOUT_S time to pop the next packet off the queue.
                # Timeout if no packet can be popped in TIMEOUT_S seconds.
                # Feed the packet into the handle packet function if one was received.
                data = self.connections[destination][1].get(block=True, timeout=TIMEOUT_S)
                handle_code, prev_packet = self.handle_packet(data, data_queue, prev_packet, destination)

                # If handle_packet() returned a 0, a fatal error occurred. Terminate the connection.
                if handle_code == 0:
                    return self.kill_connection(destination)
                # If handle_packet() returned a 2, resend the previous packet.
                elif handle_code == 2:
                    raise socket.timeout

                # If handle_packet() returned a 1, reset the number of retransmits.
                # Check to see if tftp process is complete.
                retransmits = 0

                # When satisfying an RRQ, terminate process if the last packet sent was less than 516 bytes
                # When satisfying an WRQ, terminate process if the last packet received was less than 516 bytes
                if (mode == 'w' and len(data) < BLOCK_LENGTH) or (mode == 'r' and len(self.prev_packet) < BLOCK_LENGTH):

                    # If we finished the tftp process and we were satisfying a WRQ,
                    # Open the file to be written to and write the queued data to it locking and unlocking the
                    # mutex as necessary.
                    if mode == 'w':
                        try:
                            self.mutex.acquire()
                            f = open(filename, "wb")
                            while not data_queue.empty():
                                f.write(data_queue.get())
                            f.close()
                        finally:
                            self.mutex.release()

                    # End the connection
                    print("Connection Complete...")
                    return self.kill_connection(destination)

            # Excepts timeouts and other straggling errors
            except:
                if retransmits == MAX_RETRANSMITS:  # If we have exceeded the max retransmits, send an error packet and terminate the process
                    print("ERROR: Retransmitted file {} times with no response, terminating connection.".format(MAX_RETRANSMITS))
                    self.send_err(0, destination, b'ERROR: Retransmitted too many times with no response, terminating connection.')
                    return self.kill_connection(destination)
                else:  # Otherwise, increment retransmits and resend the previous packet
                    retransmits += 1
                    self.sock.sendto(prev_packet, destination)
                    continue



