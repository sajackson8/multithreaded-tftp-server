#######################################################################################################################
# NAME: Sha Jackson
# COURSE: CSCE 365 - Computer Networks
# DATE MODIFIED: 05 DEC 2020
#
# IMPORTANT: the tftp_files directory must be stored, along with its contents, in the current working directory
#
# PURPOSE: The following program implements a TFTP server that is capable of communicating with multiple clients via
#          multithreading.
#
# DESCRIPTION: This file handles the argument parsing used to initialize and run a server with the argued server port.
#
#######################################################################################################################

from tftp_server_helper import tftp_server
import argparse

# Initializes the parser using argparse, returns a dict of the parsed arguments
def init_parser():

    parser = argparse.ArgumentParser(description='TFTP Client Parser') # Read command line args
    parser.add_argument('-sp', '--serverport', default=5001, help='The desired server port number', type=int)  # Server port argument, default = 5001, type = int
    return vars(parser.parse_args())  # Return arguments in dict


def main():

    # Stores argument in dict, user_args
    user_args = init_parser()

    # Checks to see that a valid serverport has been given
    if not 5000 <= user_args['serverport'] <= 65535:
        print('Invalid Server Port Number Parsed. Please enter a port number such that 5000 <= port <= 65535')
        exit()

    # Initializes the server (bound to server port)
    server = tftp_server("", user_args['serverport'])  # Initialize server socket

    # Starts the servers listening process
    server.listen()

main()









