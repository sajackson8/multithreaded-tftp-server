#######################################################################################################################
# NAME: Sha Jackson
# COURSE: CSCE 365 - Computer Networks
# DATE MODIFIED: 05 DEC 2020
#
# IMPORTANT: Unit tests can be run with the command line "pytest tftp_unit_tests.py"
#
# PURPOSE: The following program implements a TFTP server that is capable of communicating with multiple clients via
#          multithreading.
#
# DESCRIPTION: This file handles contains the parametrized pytest used to test the packet handling done by the server.
#
#######################################################################################################################


import pytest
from tftp_server_helper import *

test_server = tftp_server("", 5555)
TEST_DEST = ("", 5555)

no_data_queue = queue.Queue()
some_data_queue = queue.Queue()
max_data_queue = queue.Queue()

too_much_data = b'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean venenatis arcu erat, eget tempor felis dignissim in. Nulla eu metus vitae ipsum facilisis luctus. Sed lacinia vitae dolor et ultricies. Mauris a tellus ac sem aliquam auctor. Fusce ullamcorper ipsum et ornare accumsan. Ut non malesuada lacus. Aenean finibus diam vitae ex tempor interdum. Mauris efficitur mauris vitae posuere egestas. Nam porta lorem eget neque molestie scelerisque. Pellentesque tellus mi, consectetur ornare leo sed turpis duis. THIS IS BEYOND 512 BYTES OF DATA.'
max_data = b'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean venenatis arcu erat, eget tempor felis dignissim in. Nulla eu metus vitae ipsum facilisis luctus. Sed lacinia vitae dolor et ultricies. Mauris a tellus ac sem aliquam auctor. Fusce ullamcorper ipsum et ornare accumsan. Ut non malesuada lacus. Aenean finibus diam vitae ex tempor interdum. Mauris efficitur mauris vitae posuere egestas. Nam porta lorem eget neque molestie scelerisque. Pellentesque tellus mi, consectetur ornare leo sed turpis duis.'
some_data = b'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
no_data = b''

@pytest.mark.parametrize("name,data,data_queue,prev_packet,handle_code,returned_packet", [
    # RECEIVED DATA -- SEND ACK
    ("Received Valid Data -- Max Data", b'\x00\x03\x00\x01' + max_data, max_data_queue, b'\x00\x04\x00\x00', 1, b'\x00\x04\x00\x01'),
    ("Received Valid Data -- Some Data", b'\x00\x03\x00\x02' + some_data, some_data_queue, b'\x00\x04\x00\x01', 1, b'\x00\x04\x00\x02'),
    ("Received Valid Data -- No Data", b'\x00\x03\x00\x03' + no_data, no_data_queue, b'\x00\x04\x00\x02', 1, b'\x00\x04\x00\x03'),
    # RECEIVED ACK -- SEND DATA
    ("Received Valid Ack -> Max Data", b'\x00\x04\x00\x01', max_data_queue, b'\x00\x03\x00\x01' + some_data, 1, b'\x00\x03\x00\x02' + max_data),
    ("Received Valid Ack -> Some Data", b'\x00\x04\x00\x02', some_data_queue, b'\x00\x03\x00\x02' + some_data, 1, b'\x00\x03\x00\x03' + some_data),
    ("Received Valid Ack -> No Data", b'\x00\x04\x00\x03', no_data_queue, b'\x00\x03\x00\x03' + some_data, 1, b'\x00\x03\x00\x04' + no_data),
    # RECEIVED INVALID DATA -- SEND ERROR
    ("Received Data W/ Block Num < Prev Packet", b'\x00\x03\x00\x02' + some_data, no_data_queue, b'\x00\x04\x00\x03', 2, b'\x00\x04\x00\x03'),
    ("Received Data W/ Block Num == Prev Packet", b'\x00\x03\x00\x03' + some_data, no_data_queue, b'\x00\x04\x00\x03', 2, b'\x00\x04\x00\x03'),
    # RECEIVED INVALID ACK -- SEND ERROR
    ("Received Ack W/ Block Num < 2 Bytes", b'\x00\x04\x00\x01\x00', no_data_queue, b'\x00\x03\x00\x01' + some_data, 0, b'\x00\x05\x00\x04Illegal TFTP operation\x00'),
    ("Received Ack W/ Block Num > 2 Bytes", b'\x00\x04\x00', no_data_queue, b'\x00\x03\x00\x01' + some_data, 0, b'\x00\x05\x00\x04Illegal TFTP operation\x00'),
    ("Received Ack W/ Block Num < Prev Packet", b'\x00\x04\x00\x01', no_data_queue, b'\x00\x03\x00\x02' + some_data, 2, b'\x00\x03\x00\x02' + some_data),
    # RECEIVED ERROR PACKET
    ("Received Error Packet W/ Known Code", b'\x00\x05\x00\x01', no_data_queue, b'', 0, None),
    ("Received Error Packet W/ Unknown Code", b'\x00\x05\x00\x09', no_data_queue, b'', 0, None),
    # RECEIVED INVALID PACKET -- SEND ERROR
    ("Received Packet W/ Size < 2", b'\x01', no_data_queue, b'', 0, b'\x00\x05\x00\x04Illegal TFTP operation\n ERROR: Invalid Opcode (less than 2 bytes)\x00'),
    ("Received Packet W/ Size > BLOCK_LENGTH (516)", b'\x00\x03\x00\x01' + too_much_data, no_data_queue, b'', 0, b'\x00\x05\x00\x04Illegal TFTP operation\x00'),
    ("Received Packet W/ Invalid Opcode", b'\x00\x42\x00\x01', no_data_queue, b'', 0, b'\x00\x05\x00\x04Illegal TFTP operation\n ERROR: Opcode must be in range 1 to 5\x00')
])
def test_eval(name, data, data_queue, prev_packet, handle_code, returned_packet):
    assert test_server.handle_packet(data, data_queue, prev_packet, TEST_DEST, True) == (handle_code, returned_packet)

